exports.index = function(req, res) {
	if(req.session != undefined && req.session.username != undefined){
		res.render('index', {
			title: 'Login',
			username: req.session.username
		});
	}else{
		res.render('index', {
			title: 'Login',
			username: 'undefined'
		});
	}
};