/**
 * Module dependencies.
 */
var sessionTimeoutMin = 300;


var express = require('express'),
	routes = require('./routes'),
	http = require('http'),
	path = require('path');

var app = express();

var mongodb = require('mongodb');
var MongoStore = require('connect-mongostore')(express);

var sessionStore = new MongoStore({
	'db': 'supertictactoe',
	cookie: {
		maxAge: sessionTimeoutMin * 60 * 1000
	},
	"clear_interval": sessionTimeoutMin * 60,
	"host": "127.0.0.1", // required
	"port": 27017, // required
	"options": { // all optional
		"autoReconnect": false,
		"poolSize": 200,
		"socketOptions": {
			"timeout": 0,
			"noDelay": true,
			"keepAlive": 1,
			"encoding": "utf8"
		}
	},
	"collection": "sessions"
});


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));

// app.use(express.session());
app.use(express.session({
	secret: 'my secret',
	cookie: {
		maxAge: sessionTimeoutMin * 60 * 1000,
	},
	store: sessionStore
}));

app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

var socketio = require('socket.io').listen(app.listen(app.get('port')));

socketio.sockets.on('connection', function(socket) {
	console.log("Connected!");
	socket.on('gameInvite', function(data) {
		console.log("Game invite!");
		socketio.sockets.emit("gameInvite", data);
	});
	socket.on('opponentMadeMove', function(data) {
		console.log("Opponent made move!");
		socketio.sockets.emit("opponentMadeMove", data);
	});
	socket.on('inviteAccepted', function(data) {
		console.log("The invite was accepted");
		socketio.sockets.emit("inviteAccepted", data);
	});
	socket.on('quitGame', function(data) {
		console.log("A game has been quit");
		socketio.sockets.emit("quitGame", data);
	});
	socket.on('logout', function(data) {
		console.log("A player has logged out");
		socketio.sockets.emit("quitGame", data);
	});
});


app.get('/', routes.index);

app.post('/tryAcquireUsername', function(req, res) {
	new mongodb.Db('supertictactoe', new mongodb.Server("127.0.0.1", 27017, {
		safe: true
	}), {}).open(function(error, client) {
		var collection = new mongodb.Collection(client, "sessions");
		collection.find({
			"session.username": req.param('username')
		}).toArray(function(err, sessions) {
			if (err) {
				res.end("false");
				return;
			}
			if (sessions.length != 0) {
				console.log("Found " + sessions.length + "sessions with that username");
				res.end("false");
			} else if (req.session != undefined) {
				req.session.username = req.param('username');
				req.session.isPlaying = false;
				res.end("true");
			} else {
				console.log("req.session was undefined?!");
				res.end("false");
			}
		});
	});
});

app.post('/isUserOnline', function(req, res) {
	new mongodb.Db('supertictactoe', new mongodb.Server("127.0.0.1", 27017, {
		safe: true
	}), {}).open(function(error, client) {
		var collection = new mongodb.Collection(client, "sessions");
		collection.find({
			"session.username": req.param('username')
		}).toArray(function(err, sessions) {
			if (err) {
				res.end("false");
				return;
			}
			if (sessions.length != 0) {
				res.end("false");
			}
			res.end("true");
		});
	});
});

app.post('/getListOfUsers', function(req, res) {
	new mongodb.Db('supertictactoe', new mongodb.Server("127.0.0.1", 27017, {
		safe: true
	}), {}).open(function(error, client) {
		var collection = new mongodb.Collection(client, "sessions");
		collection.find().toArray(function(err, sessions) {
			res.end(JSON.stringify(sessions));
		});
	});
});

app.post('/logout', function(req, res) {
	console.log("Logging out the user");
	if (req.session.username == undefined) {
		console.log("The username was already undefined?!");
	}
	req.session.username = undefined;
	res.end(JSON.stringify({
		success: 1
	}));
});

// http.createServer(app).listen(app.get('port'), function() {
// 	console.log('Express server listening on port ' + app.get('port'));
// });