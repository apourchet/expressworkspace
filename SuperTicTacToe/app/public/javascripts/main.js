var ctx = null;
var board = null;
var player = 0;
var username = null;
var opponentName = null;
var isInGame = false;
var socket = io.connect('http://localhost:3000');

function Board(context) {
	console.log("new Board");
	this.activeSubBoard = -1;
	this.ctx = context;
	this.subBoards = new Array();
	this.turn = -1;
	this.completedSubboards = new Array(-3, -3, -3, -3, -3, -3, -3, -3, -3);
	for (var i = 0; i < 9; i++) {
		this.subBoards.push(new Array(-3, -3, -3, -3, -3, -3, -3, -3, -3));
	}
	console.log("The new board has been initialized");
	this.drawBoard();
}

Board.prototype.drawBoard = function() {
	console.log("drawBoard");
	ctx.clearRect(0, 0, 450, 450);
	ctx.strokeStyle = "#000000";
	ctx.moveTo(0, 0);
	ctx.lineTo(450, 0);
	ctx.lineTo(450, 450);
	ctx.lineTo(0, 450);
	ctx.lineTo(0, 0);
	for (var x = 0; x < 8; x++) {
		ctx.moveTo(50 * (x + 1), 0);
		ctx.lineTo(50 * (x + 1), 450);
		if (x % 3 == 2) {
			ctx.moveTo(50 * (x + 1) + 2, 0);
			ctx.lineTo(50 * (x + 1) + 2, 450);
			ctx.moveTo(50 * (x + 1) - 2, 0);
			ctx.lineTo(50 * (x + 1) - 2, 450);
		}
	}
	for (var x = 0; x < 9; x++) {
		ctx.moveTo(0, 50 * (x + 1));
		ctx.lineTo(450, 50 * (x + 1));
		if (x % 3 == 2) {
			ctx.moveTo(0, 50 * (x + 1) + 2);
			ctx.lineTo(450, 50 * (x + 1) + 2);
			ctx.moveTo(0, 50 * (x + 1) - 2);
			ctx.lineTo(450, 50 * (x + 1) - 2);
		}
	}
	ctx.stroke();
	ctx.strokeStyle = "#ff0000";
	ctx.beginPath();
	for (var i = 0; i < this.subBoards.length; i++) {
		for (var j = 0; j < 9; j++) {
			if (this.subBoards[i][j] >= 0) {
				if (this.subBoards[i][j] == 1) {
					// console.log("Drawing this 1");
					ctx.moveTo((i % 3) * 150 + (j % 3) * 50, Math.floor(i / 3) * 150 + Math.floor(j / 3) * 50);
					ctx.lineTo((i % 3) * 150 + (j % 3) * 50 + 50, Math.floor(i / 3) * 150 + Math.floor(j / 3) * 50 + 50);

					ctx.moveTo((i % 3) * 150 + (j % 3) * 50, Math.floor(i / 3) * 150 + Math.floor(j / 3) * 50 + 50);
					ctx.lineTo((i % 3) * 150 + (j % 3) * 50 + 50, Math.floor(i / 3) * 150 + Math.floor(j / 3) * 50);
				} else {
					// console.log("Drawing this 0");
					ctx.moveTo((i % 3) * 150 + (j % 3) * 50 + 46, Math.floor(i / 3) * 150 + Math.floor(j / 3) * 50 + 25);
					ctx.arc((i % 3) * 150 + (j % 3) * 50 + 24, Math.floor(i / 3) * 150 + Math.floor(j / 3) * 50 + 24, 21, 0, 2 * Math.PI, false);
				}
			}
		};
	};
	ctx.stroke();
	ctx.beginPath();
	for (var i = 0; i < this.subBoards.length; i++) {
		if (this.completedSubboards[i] >= 0) {
			// Draw the finished subboard
			if (this.completedSubboards[i] == 1) {
				console.log("Drawing this 1");
				ctx.moveTo((i % 3) * 150, Math.floor(i / 3) * 150);
				ctx.lineTo((i % 3) * 150 + 150, Math.floor(i / 3) * 150 + 150);

				ctx.moveTo((i % 3) * 150, Math.floor(i / 3) * 150 + 150);
				ctx.lineTo((i % 3) * 150 + 150, Math.floor(i / 3) * 150);
			} else {
				console.log("Drawing this 0");
				ctx.moveTo((i % 3) * 150 + 145, Math.floor(i / 3) * 150 + 75);
				ctx.arc((i % 3) * 150 + 75, Math.floor(i / 3) * 150 + 75, 70, 0, 2 * Math.PI, false);
			}
		}
	};
	ctx.stroke();
	if (this.turn == player) {
		// Draw the active subboard

	}
	ctx.stroke();
}

Board.prototype.isSubboardCompleted = function(index) {
	if (index > 8) {
		console.log("This subboard does not exist.");
		return true;
	}
	console.log("isSubboardCompleted " + index);
	var b = this.subBoards[index];
	// Check colums
	if (b[0] + b[3] + b[6] == 0 || b[0] + b[3] + b[6] == 3) {
		return true;
	}
	if (b[1] + b[4] + b[7] == 0 || b[1] + b[4] + b[7] == 3) {
		return true;
	}
	if (b[2] + b[5] + b[8] == 0 || b[2] + b[5] + b[8] == 3) {
		return true;
	}

	// Check rows
	if (b[0] + b[1] + b[2] == 0 || b[0] + b[1] + b[2] == 3) {
		return true;
	}
	if (b[3] + b[4] + b[5] == 0 || b[3] + b[4] + b[5] == 3) {
		return true;
	}
	if (b[6] + b[7] + b[8] == 0 || b[6] + b[7] + b[8] == 3) {
		return true;
	}

	// Check diagnals
	if (b[0] + b[4] + b[8] == 0 || b[0] + b[4] + b[8] == 3) {
		return true;
	}
	if (b[2] + b[4] + b[6] == 0 || b[2] + b[4] + b[6] == 3) {
		return true;
	}
	console.log("Nope, that subboard was not completed " + (b[0] + ";" + b[3] + ";" + b[6]));
	return false;
}

Board.prototype.isBoardCompleted = function() {
	var b = this.completedSubboards;

	// Check colums
	if (b[0] + b[3] + b[6] == 0 || b[0] + b[3] + b[6] == 3) {
		return true;
	}
	if (b[1] + b[4] + b[7] == 0 || b[1] + b[4] + b[7] == 3) {
		return true;
	}
	if (b[2] + b[5] + b[8] == 0 || b[2] + b[5] + b[8] == 3) {
		return true;
	}

	// Check rows
	if (b[0] + b[1] + b[2] == 0 || b[0] + b[1] + b[2] == 3) {
		return true;
	}
	if (b[3] + b[4] + b[5] == 0 || b[3] + b[4] + b[5] == 3) {
		return true;
	}
	if (b[6] + b[7] + b[8] == 0 || b[6] + b[7] + b[8] == 3) {
		return true;
	}

	// Check diagnals
	if (b[0] + b[4] + b[8] == 0 || b[0] + b[4] + b[8] == 3) {
		return true;
	}
	if (b[2] + b[4] + b[6] == 0 || b[2] + b[4] + b[6] == 3) {
		return true;
	}

	return false;
}

Board.prototype.reset = function() {
	this.activeSubBoard = -1;
	this.subBoards = new Array();
	this.turn = -1;
	for (var i = 0; i < 9; i++) {
		this.subBoards.push(new Array(-3, -3, -3, -3, -3, -3, -3, -3, -3));
	}
	console.log("The new board has been initialized");
	this.drawBoard();
};

Board.prototype.isValidMove = function(coord) {
	console.log("isValidMove");
	var indices = Board.getCoordIndices(coord);
	var subBoardIndex = indices.subBoardIndex;
	if (this.activeSubBoard == -1 || this.activeSubBoard == subBoardIndex) {
		var boxIndex = indices.boxIndex;
		if (this.subBoards[subBoardIndex][boxIndex] < 0) {
			console.log("Move is valid");
			return true;
		} else {
			// Someone has already placed something in this box
			console.log("Box is not empty");
			return false;
		}
	} else {
		// Cannot make a move on this subboard
		console.log("Cannot make a move on this subboard");
		showQuickDialog("You cannot make a move on this subboard!");
		return false;
	}
	return false;
}

Board.prototype.isYourTurn = function(p) {
	return this.turn == p;
}

Board.prototype.onClick = function(coord, p) {
	console.log("Board.onClick " + coord.x + "; " + coord.y);
	if (isInGame == false) {
		showQuickDialog("You are not in a game!");
		return;
	}
	if (!this.isYourTurn(p)) {
		// Is not your turn to play
		console.log("It is not your turn to play. You are player " + p + " and it is the turn of player " + this.turn);
		showQuickDialog("It is not your turn to play.");
		return;
	}
	if (this.isValidMove(coord)) {
		this.makeMove(coord, p);
	} else {
		console.log("Cannot move there");
		// Cannot move there
	}
}

Board.prototype.printSubBoard = function(index) {
	var b = this.subBoards[index];
	console.log("SubBoard " + index + ": " + b[0] + ";" + b[1] + ";" + b[2]);
	console.log("SubBoard " + index + ": " + b[3] + ";" + b[4] + ";" + b[5]);
	console.log("SubBoard " + index + ": " + b[6] + ";" + b[7] + ";" + b[8]);
}

Board.prototype.changeBox = function(coord, p) {
	var indices = Board.getCoordIndices(coord);
	var isNowFull = true;
	for (var i = 0; i < 9; i++) {
		if (this.subBoards[indices.subBoardIndex][i] < 0) {
			isNowFull = false;
			break;
		}
	};

	this.subBoards[indices.subBoardIndex][indices.boxIndex] = p;
	// this.printSubBoard(indices.subBoardIndex);
	console.log("Changed box " + indices.subBoardIndex + "; " + indices.boxIndex);
	if (this.isSubboardCompleted(indices.subBoardIndex) && this.completedSubboards[indices.subBoardIndex] < 0) {
		this.completedSubboards[indices.subBoardIndex] = p;
		console.log("A subBoard has been completed");
		if (p == player) {
			showQuickDialog("You have finished a subboard!");
		} else {
			showQuickDialog("Your opponent has finished a subboard!");
		}
	}
	if (this.isBoardCompleted()) {
		// Game is over
		if (p == player) {
			showQuickDialog("You won the game!");
		} else {
			showQuickDialog("Your opponent has won the game!");
		}
	}

	if (isNowFull) {
		console.log("The targetted subBoard is full");
		this.activeSubBoard = -1;
	} else {
		this.activeSubBoard = indices.boxIndex;
	}
	this.drawBoard();
}

Board.prototype.makeMove = function(coord, p) {
	console.log("Board.makeMove");
	this.changeBox(coord, p);
	this.turn = (this.turn+1) % 2;
	console.log("board turn set to " + board.turn + ". Player is " + player);
	socket.emit('opponentMadeMove', JSON.stringify({
		opponentName: opponentName,
		coords: coord
	}));
}

Board.getCoordIndices = function(coord) {
	return {
		subBoardIndex: Math.floor(coord.x / 3) + 3 * Math.floor(coord.y / 3),
		boxIndex: (coord.x % 3) + 3 * (coord.y % 3)
	};
}

function tryAcquireUsername(username, callback) {
	console.log("tryAcquireUsername");
	$.post('/tryAcquireUsername', {
		username: username
	}, function(data) {
		console.log("Got response from /tryAcquireUsername: " + data);
		callback(data);
	});
}

function isUserOnline(username, callback) {
	console.log("isUserOnline");
	$.post('/isUserOnline', {
		username: username
	}, function(data) {
		console.log("Got response from /isUserOnline: " + data);
		callback(data);
	});
}

function getListOfUsers(callback) {
	console.log("getListOfUsers");
	$.post('/getListOfUsers', function(data) {
		console.log("Got response from /getListOfUsers: " + data);
		callback(data);
	});
}

function setupLoginPage() {
	console.log("setupLoginPage");
	$(document.body).html("");
	$(document.body).append('<h1>Welcome </h1><p>Choose</p><input type="text" id="usernameInput" placeholder="username"><button id="usernameButton">Log in</button>');
	$('#usernameButton').click(function() {
		console.log("Need to verify that the username '" + $("#usernameInput").val() + "' is not taken");
		tryAcquireUsername($("#usernameInput").val(), function(data) {
			if (data == "true") {
				console.log("Logged in as " + $("#usernameInput").val());
				username = $("#usernameInput").val();
				setupGamePage();
			} else {
				console.log("The username " + $("#usernameInput").val() + " was already taken");
			}
		});
	});
}

function setupGamePage() {
	console.log("setupGamePage");
	$(document.body).html("");
	$(document.body).append("<h1>...</h1><canvas id='mCanvas' width=450 height=450></canvas>");
	ctx = document.getElementById('mCanvas').getContext('2d');
	board = new Board(ctx);
	$('#mCanvas').click(function(e) {
		var x;
		var y;
		if (e.pageX || e.pageY) {
			x = e.pageX;
			y = e.pageY;
		} else {
			x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
			y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
		}
		x -= document.getElementById('mCanvas').offsetLeft;
		y -= document.getElementById('mCanvas').offsetTop;
		console.log("Clicked on the canvas at position " + x + "; " + y);
		board.onClick({
			x: Math.floor(x / 50),
			y: Math.floor(y / 50)
		}, player);
	});
	getListOfUsers(function(data) {
		$(document.body).append("<div id='mAccordion'>");
		var data = JSON.parse(data);
		console.log(data);
		for (var i = 0; i < data.length; i++) {
			if (data[i].session.username != username && data[i].session.isPlaying == false) {
				$("#mAccordion").append("<h3>" + data[i].session.username + "</h3>");
				$("#mAccordion").append("<div><button onclick=\"gameInvite('" + data[i].session.username + "')\"> Start a game </button></div>");
			}
		}
		$("#mAccordion").accordion();
		$("input[type=submit], a, button")
			.button()
			.click(function(event) {
			event.preventDefault();
		});
	});
	$(document.body).append("<button id='logoutButton' onclick='logout()'>Log out</button>");
	$("input[type=submit], a, button")
		.button()
		.click(function(event) {
		event.preventDefault();
	});
}

function logout() {
	console.log("Attempting to log out of the game.");
	$.post('/logout', function(data) {
		if (JSON.parse(data).success == 1) {
			socket.emit('logout', username);
		}
	});
}

function gameInvite(name) {
	console.log("Sending invite to " + name);
	showQuickDialog("Sending invite to " + name);
	socket.emit("gameInvite", JSON.stringify({
		p1: username,
		p2: name
	}));
}

function acceptInvite(name) {
	$("#inviteDialog").dialog('close');
	setTimeout(function() {
		$("#inviteDialog").remove();
	}, 1000);
	console.log("You accepted the invitation! The game with " + name + " has started!");
	socket.emit("inviteAccepted", username);
	opponentName = name;
	board.turn = 0;
	console.log("board turn set to " + board.turn + ". Player is " + player);
	player = 0;
	isInGame = true;
	$(document.body).append("<button id='quitGameButton' onclick='quitGameDialog()'>Quit the game</button>");
	$("input[type=submit], a, button")
		.button()
		.click(function(event) {
		event.preventDefault();
	});

}

function denyInvite() {
	$("#inviteDialog").dialog('close');
	setTimeout(function() {
		$("#inviteDialog").remove();
	}, 1000);
}

function quitGameDialog() {
	console.log("Displaying quit game dialog");
	var dial = document.createElement("div");
	dial.title = "Quit Game?";
	dial.id = "quitDialog";
	dial.innerHTML = "Are you sure you want to quit the game with " + opponentName + "<br /><button onclick=\"quitGame()\">Yes</button><button onclick=\"cancelQuitDialog()\">No</button>";
	$(dial).dialog({
		hide: {
			effect: "fadeOut",
			duration: 2000
		}
	});
	$("input[type=submit], a, button")
		.button()
		.click(function(event) {
		event.preventDefault();
	});
}

function quitGame() {
	$("#quitDialog").dialog('close');
	console.log("The game will quit...");
	socket.emit("quitGame", username);
	$("#quitGameButton").remove();
	isInGame = false;
	setTimeout(function() {
		$("#quitDialog").remove();
	}, 1000);
}

function cancelQuitDialog() {
	$("#quitDialog").dialog('close');
	setTimeout(function() {
		$("#quitDialog").remove();
	}, 1000);
}

function showQuickDialog(string, time) {
	var dial = document.createElement("div");
	dial.title = "Message";
	var rId = Math.floor(Math.random() * 1000);
	dial.id = "quickDialog_" + rId;
	dial.innerHTML = string;
	$(dial).dialog({
		hide: {
			effect: "fadeOut",
			duration: 700
		}
	});
	$("input[type=submit], a, button")
		.button()
		.click(function(event) {
		event.preventDefault();
	});
	setTimeout(function() {
		$("#quickDialog_" + rId).dialog('close');
		setTimeout(function() {
			$("#quickDialog_" + rId).remove();
		}, (time == undefined) ? 2000 : time);
	}, (time == undefined) ? 2000 : time);
}

socket.on('logout', function(data) {
	if (data == opponentName) {
		console.log("Your opponent has logged out of the game. The game has been cancelled permanently.");
		board.reset();
		isInGame = false;
	} else if (data == username) {
		console.log("You have been successfully logged out of the game.");
		board.reset();
		isInGame = false;
		$("#logoutButton").remove();
		setupLoginPage();
	}
});

socket.on('inviteAccepted', function(data) {
	if (data == username) {
		console.log("Caught our own event.");
		return;
	}
	console.log("Our invitation was accepted! The game with " + data + " has started!");
	opponentName = data;
	board.turn = 0;
	console.log("board turn set to " + board.turn + ". Player is " + player);
	player = 1;
	isInGame = true;
	$(document.body).append("<button id='quitGameButton' onclick='quitGameDialog()'>Quit the game</button>");
	$("input[type=submit], a, button")
		.button()
		.click(function(event) {
		event.preventDefault();
	});
});

socket.on('opponentMadeMove', function(data) {
	data = JSON.parse(data);
	if (data.opponentName == username) {
		console.log("The opponent made a move: " + data.coords.x + "; " + data.coords.y);
		board.changeBox(data.coords, board.turn);
		board.turn = player;
		console.log("board turn set to " + board.turn + ". Player is " + player);
	} else {
		if (data.opponentName == opponentName) {
			console.log("Caught our own event");
		} else {
			console.log("Not our game");
		}
	}
});

socket.on('quitGame', function(data) {
	if (data == username || data == opponentName) {
		console.log("The game has been canceled");
		board.reset();
		isInGame = false;
		$("#quitGameButton").remove();
		return;
	}
});

socket.on('gameInvite', function(data) {
	console.log("Socket has detected game invite.");
	console.log("Invitation: " + data);
	data = JSON.parse(data);
	if (data.p1 == username) {
		console.log("Lol thats our own game invite");
	}
	if (data.p2 == username) {
		console.log("We have received an invite from " + data.p1 + ". Do we want to accept?");
		if (isInGame == false) {
			var dial = document.createElement("div");
			dial.title = "Game invite from " + data.p1;
			dial.id = "inviteDialog";
			dial.innerHTML = "" + data.p1 + " wants to play with you. <br />Do you accept?<br /><button onclick=\"acceptInvite('" + data.p1 + "')\">Accept</button><button onclick=\"denyInvite()\">Deny</button>";
			$(dial).dialog({
				hide: {
					effect: "fadeOut",
					duration: 1000
				}
			});
			$("input[type=submit], a, button")
				.button()
				.click(function(event) {
				event.preventDefault();
			});
		}
	} else {
		console.log("The invite is not for us (" + data.p2 + "!=" + username + ")");
	}
});

$(document).ready(function() {
	console.log("document.ready");
});