$.fn.extend({ 
    disableSelection: function() { 
        this.each(function() { 
            if (typeof this.onselectstart != 'undefined') {
                this.onselectstart = function() { return false; };
            } else if (typeof this.style.MozUserSelect != 'undefined') {
                this.style.MozUserSelect = 'none';
            } else {
                this.onmousedown = function() { return false; };
            }
        }); 
    } 
});


var canvas = null;
var socket = io.connect('http://localhost:3000');
var clicking = false;

function doColor(x, y){
	canvas.fillStyle = "rgb(255,255,255)";
	canvas.fillRect(x*20, y*20, 20, 20);
}

socket.on('draw', function(data){
	console.log("Draw function called on client socket.");
	doColor(data.x, data.y);
});

$(document).ready(function(){
	$(document).mousedown(function(){
		clicking = true;
	});
	$(document).mouseup(function(){
		clicking = false;
	});

	console.log("JQuery is running");
	canvas = document.getElementById("mCanvas").getContext('2d');
	console.log("Got canvas context");
    $(document).disableSelection();            

	$("#mCanvas").mousemove(function(e){
		if(clicking){
			var x = Math.floor((e.pageX-$("#mCanvas").offset().left) / 20);
		    var y = Math.floor((e.pageY-$("#mCanvas").offset().top) / 20);
		    doColor(x, y);
		    socket.emit('draw', {x:x, y:y});
		}else{
			console.log("Not clicking");
		}
	});
});