var glEnv;
var glLight;
var glCamera;

var angle = 0;

function webGLStart() {
	console.log("Starting WebGL");

	glLight = new GLLight(true, [0.2, 0.2, 0.2], [1, 1, 1], [0, 0, 0], [0, -2, -1], [5, 20, 20]);
	glCamera = new GLCamera([0.5, 0.5, 2], [0.5, 0.5, 0], [0, 1, 0]);
	glEnv = new GLEnv(document.body, 600, 600, 1000, false, glLight, glCamera);

	glEnv.on(GLEnv.EVENT_INIT, function(gl) {
		console.log("GLEvent init");
		gl.clearColor(0.3, 0.3, 0.3, 1.0); // Set clear color to black, fully opaque
		gl.enable(gl.DEPTH_TEST); // Enable depth testing
		gl.depthFunc(gl.LEQUAL); // Near things obscure far things
	});

	glEnv._construct();

	GLTextureLoader.addToLoadStack('/images/img1.jpg');
	GLFileLoader.addToLoadStack('/objects/test.obj');
	GLFileLoader.addToLoadStack('/objects/teapot.obj');

	var mesh;
	glEnv.on(GLEnv.EVENT_ASSETLOAD, function() {
		console.log("GLEnv assetload");
		// mesh = new GLEnv.GLPrimitiveMeshes.TexturedSquare(glEnv, 10, '/images/img1.jpg');
		// mesh = new GLEnv.GLPrimitiveMeshes.TexturedCube(glEnv, 1, '/images/img1.jpg');
		mesh = new GLEnv.GLPrimitiveMeshes.PointCloud(glEnv, '/objects/teapot.obj');
	});

	glEnv.on(GLEnv.EVENT_DRAW, function(gl) {
		angle += 0.01;

		// glEnv.light.position[0] = x;
		// glEnv.camera.eye[0] = Math.sin(45) * 60;
		// glEnv.camera.eye[1] = 30;
		// glEnv.camera.eye[2] = Math.cos(45) * 60;

		// glEnv.light.position[0] = Math.sin(angle) * 50;
		// glEnv.light.position[1] = 10;
		// glEnv.light.position[2] = Math.cos(angle) * 50;

		glEnv.camera.eye[0] = Math.sin(angle) * 8;
		glEnv.camera.eye[1] = 2;
		glEnv.camera.eye[2] = Math.cos(angle) * 8;

		// for (var j = 0; j < 20; j += 2) {
		// 	mat4.translate(glEnv.mvMatrix, [0, j - 10, 0]);
		// 	for (var i = 0; i < 30; i += 2) {
		// 		mat4.translate(glEnv.mvMatrix, [i - 15, 0, 0]);
		// mat4.translate(glEnv.mvMatrix, [60, -2, -3]);
		// var tst = mat4.create();
		// mat4.identity(tst);
		// mat4.translate(tst, [30, -2, -3]);
		mesh.draw();
		// 		mat4.translate(glEnv.mvMatrix, [-(i - 15), 0, 0]);
		// 	}
		// 	mat4.translate(glEnv.mvMatrix, [0, -(j - 10), 0]);
		// }
	});

	glEnv.useProgram('/shaders/vertex_2.sl', '/shaders/fragment_1.sl', {
		pos: {
			name: 'aPos',
			count: 3,
			type: 'attribute',
		},
		color: {
			name: 'aColor',
			count: 4,
			type: 'attribute'
		},
		texture: {
			name: 'aTex',
			count: 2,
			type: 'attribute'
		},
		normal: {
			name: 'aNorm',
			count: 3,
			type: 'attribute'
		},
		pMatrix: {
			name: 'uPMatrix',
			count: 1,
			type: 'uniform',
			uniformType: 'mat4'
		},
		mvMatrix: {
			name: 'uMVMatrix',
			count: 1,
			type: 'uniform',
			uniformType: 'mat4'
		},
		useLighting: {
			name: 'uUseLighting',
			count: 1,
			type: 'uniform',
			uniformType: 'bool'
		},
		usePointLighting: {
			name: 'uUsePointLighting',
			count: 1,
			type: 'uniform',
			uniformType: 'bool'
		},
		ambientColor: {
			name: 'uAmbientColor',
			count: 3,
			type: 'uniform',
			uniformType: 'vec3'
		},
		lightingColor: {
			name: 'uLightingColor',
			count: 3,
			type: 'uniform',
			uniformType: 'vec3'
		},
		lightingDirection: {
			name: 'uLightingDirection',
			count: 3,
			type: 'uniform',
			uniformType: 'vec3'
		},
		lightingPosition: {
			name: 'uLightingPosition',
			count: 3,
			type: 'uniform',
			uniformType: 'vec3'
		},
		objMatrix: {
			name: 'uObjMatrix',
			count: 1,
			type: 'uniform',
			uniformType: 'mat4'
		},
		pointSize: {
			name: 'pointSize',
			count: 1,
			type: 'uniform',
			uniformType: 'float'
		},
		maxLightRange: {
			name: 'maxLightRange',
			count: 1,
			type: 'uniform',
			uniformType: 'float'
		},
	}, function() {
		console.log("Starting the rendering");
		glEnv.render();
	});
}

$(document).ready(function() {
	console.log("Ready");
	webGLStart();
});