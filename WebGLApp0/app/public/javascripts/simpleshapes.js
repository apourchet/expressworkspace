var glEnv;
var glLight;
var glCamera;

var inc = true;
var x = -10;
function webGLStart() {
	console.log("Starting WebGL");

	glLight = new GLLight(true, [0.0, 0.0, 0.0], [1, 1, 1], [0, 0, 0], [2, 2, -1], [0, 0, 1]);
	glCamera = new GLCamera([5, 0, 40], [0, 0, 0], [0, 1, 0]);
	glEnv = new GLEnv(document.body, 600, 600, GLEnv.RENDERMODE_CONTINUOUSLY, true, glLight, glCamera);

	glEnv.on(GLEnv.EVENT_INIT, function(gl) {
		gl.clearColor(0.3, 0.3, 0.3, 1.0); // Set clear color to black, fully opaque
		gl.enable(gl.DEPTH_TEST); // Enable depth testing
		gl.depthFunc(gl.LEQUAL); // Near things obscure far things
	});

	glEnv._construct();

	var mesh = new GLMesh(glEnv, false, false, [
		0, 0, 0,
		1, 0, 0,
		1, 1, 0
	], [
		1.0, 0.0, 0.0, 1.0,
		0.0, 1.0, 0.0, 1.0,
		0.0, 0.0, 1.0, 1.0
	], [], []);
	mesh._construct();

	var mesh1 = new GLMesh(glEnv, false, false, [
		0, 1, 0,
		1, 1, 0,
		1, 2, 0,

		0, 1, 0,
		1, 2, 0,
		0, 2, 0
	], [
		1.0, 0.0, 0.0, 1.0,
		1.0, 1.0, 0.0, 1.0,
		1.0, 1.0, 1.0, 1.0,

		1.0, 0.0, 0.0, 1.0,
		1.0, 1.0, 1.0, 1.0,
		1.0, 0.5, 0.2, 1.0
	], [], []);
	mesh1._construct();

	var mesh2 = new GLMesh(glEnv, true, false, [
		0, 2, 0,
		1, 2, 0,
		1, 3, 0
	], [], [
		0, 0,
		1, 0,
		1, 1
	], [], null, '/images/img1.jpg');
	mesh2._construct();

	var mesh3 = new GLMesh(glEnv, true, true, [
		0, 3, 0,
		1, 3, 0,
		1, 4, 0
	], [], [
		0, 0,
		1, 0,
		1, 1
	], [
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
	], null, '/images/img1.jpg');
	mesh3._construct();

	var mesh4 = new GLMesh(glEnv, true, true, [
		0, 0, 0,
		0, 1, 0,
		1, 0, 0,
		1, 1, 0
	], [], [
		0, 0,
		0, 1,
		1, 0,
		1, 1
	], [
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
	], glEnv.gl.TRIANGLE_STRIP, '/images/img1.jpg');
	mesh4._construct();

	glEnv.on(GLEnv.EVENT_DRAW, function(gl) {
		for (var j = 0; j < 20; j++) {
			mat4.translate(glEnv.mvMatrix, [0, j - 10, 0]);
			for (var i = 0; i < 30; i++) {
				mat4.translate(glEnv.mvMatrix, [i - 15, 0, 0]);
				// glEnv.testDraw();
				// mesh.draw();
				// mesh1.draw();
				// mesh2.draw();
				mesh4.draw();
				mat4.translate(glEnv.mvMatrix, [-(i - 15), 0, 0]);
			}
			mat4.translate(glEnv.mvMatrix, [0, -(j - 10), 0]);
		}
	}, 'testFunction');

	// glEnv.useProgram('shader-vs', 'shader-fs', {
	glEnv.useProgram('/shaders/vertex_1.sl', '/shaders/fragment_1.sl', {
		pos: {
			name: 'aPos',
			count: 3,
			type: 'attribute'
		},
		color: {
			name: 'aColor',
			count: 4,
			type: 'attribute'
		},
		texture: {
			name: 'aTex',
			count: 2,
			type: 'attribute'
		},
		normal: {
			name: 'aNorm',
			count: 3,
			type: 'attribute'
		},
		pMatrix: {
			name: 'uPMatrix',
			count: 1,
			type: 'uniform'
		},
		mvMatrix: {
			name: 'uMVMatrix',
			count: 1,
			type: 'uniform'
		},
		useLighting: {
			name: 'uUseLighting',
			count: 1,
			type: 'uniform'
		},
		usePointLighting: {
			name: 'uUsePointLighting',
			count: 1,
			type: 'uniform'
		},
		ambientColor: {
			name: 'uAmbientColor',
			count: 3,
			type: 'uniform'
		},
		lightingColor: {
			name: 'uLightingColor',
			count: 3,
			type: 'uniform'
		},
		lightingDirection: {
			name: 'uLightingDirection',
			count: 3,
			type: 'uniform'
		},
		lightingPosition: {
			name: 'uLightingPosition',
			count: 3,
			type: 'uniform'
		},
		eyePosition: {
			name: 'uEyePosition',
			count: 3,
			type: 'uniform'
		}
	}, function() {
		glEnv.render();
	});
}

$(document).ready(function() {
	console.log("Ready");
	webGLStart();
});