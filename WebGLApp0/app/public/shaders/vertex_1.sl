attribute vec3 aPos;
attribute vec4 aColor;
attribute vec2 aTex;
attribute vec3 aNorm;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat3 uNMatrix;

uniform vec3 uAmbientColor;
uniform vec3 uLightingColor;
uniform vec3 uLightingDirection;
uniform vec3 uLightingPosition;

uniform vec3 uEyePosition;

uniform bool uUseLighting;
uniform bool uUsePointLighting;

varying vec4 vColor;
varying vec2 vTex;
varying vec3 vNorm;
varying vec3 vLightWeight;

float maxLightRange = (5.0);

void main(void) {
    vec4 mvPosition = uMVMatrix * vec4(aPos, 1.0);

	gl_Position = uPMatrix * mvPosition;
	vColor = aColor;
	vTex = aTex;
	vNorm = aNorm;

	if (!uUseLighting) {
		vLightWeight = vec3(1.0, 1.0, 1.0);
	} else {
		if(uUsePointLighting){
			vec3 lightDirection = normalize(mvPosition.xyz - uLightingPosition);
            vec3 transformedNormal = uNMatrix * aNorm;

            float theDistance = distance(uLightingPosition, mvPosition.xyz + uEyePosition);
            float diminish = pow((theDistance / maxLightRange), 2.0);

            float directionalLightWeighting = max(dot(transformedNormal, normalize(lightDirection) * vec3(-1, -1, -1)), 0.0);
            if(diminish < 0.7){
            	diminish = 0.7;
        	} 
            vLightWeight = uAmbientColor + uLightingColor * directionalLightWeighting / (diminish);
        	

            if(directionalLightWeighting == 0.0){
            	vLightWeight = vec3(1.0, 0.0, 0.0);
            }

		}else{
			vec3 transformedNormal = uNMatrix * aNorm;
			float directionalLightWeighting = max(dot(transformedNormal, normalize(uLightingDirection) * vec3(-1, -1, -1)), 0.0);
			vLightWeight = uAmbientColor + uLightingColor * directionalLightWeighting;

			if(directionalLightWeighting == 0.0){
            	vLightWeight = vec3(1.0, 0.0, 0.0);
            }
		}
	}
	if(aNorm == vec3(0,0,0)){
		vLightWeight = vec3(1, 1, 1);
	}
	if(uLightingDirection== vec3(0, 0, 0) && !uUsePointLighting){
		vLightWeight = vec3(1, 1, 1);
	}

}