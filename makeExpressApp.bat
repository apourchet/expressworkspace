# The plugins that are installed by default are: stylus, jade, socket.io, mongoose
mkdir %1 && cd %1 && npm install express && node_modules\.bin\express --css stylus --sessions app && move node_modules app && cd app && npm install stylus && npm install jade && npm install socket.io && npm install mongodb && cd ../.. && del "makeExpressApp.bat~" && cd %1/app && start node app.js
